using System;
using System.Runtime.Remoting;
using Resto.Front.Api.Attributes.JetBrains;
using Resto.Front.Api.Data.Orders;
using Resto.Front.Api.OperationContexts;
using Resto.Front.Api.UI;

namespace Resto.Front.Api.SampleScalePlugin
{
    internal sealed class NavigationNotification : IDisposable
    {
        private readonly IDisposable subscriptions;
        
        public NavigationNotification()
        {
            subscriptions =
                PluginContext.Notifications.NavigatingToPaymentScreen.Subscribe(x =>
                    OnNavigation(x.order, x.os, x.vm, x.context));
        }

        private void OnNavigation([NotNull] IOrder order, [NotNull] IOperationService os, [NotNull] IViewManager vm,
            INavigatingToPaymentScreenOperationContext context)
        {
            // https://github.com/iiko/front.api.sdk/blob/master/sample/v8preview5/Resto.Front.Api.SamplePlugin/NotificationHandlers/NavigatingToPaymentScreenHandler.cs   
        }
        
        public void Dispose()
        {
            try
            {
                subscriptions.Dispose();
            }
            catch (RemotingException)
            {
                // nothing to do with the lost connection
            }
        }
    }
}