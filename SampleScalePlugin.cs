﻿using System;
using System.Collections.Generic;
using Resto.Front.Api.Attributes;
using Resto.Front.Api.Attributes.JetBrains;

namespace Resto.Front.Api.SampleScalePlugin
{
    [UsedImplicitly]
    [PluginLicenseModuleId(0021023201)]
    public sealed class SampleScalePlugin : IFrontPlugin
    {
        private readonly List<IDisposable> subscriptions;

        public SampleScalePlugin()
        {
            subscriptions = new List<IDisposable>();
            PluginContext.Operations.AddButtonToOrderEditScreen("Discount First",
                x => DiscountSystem.AddSelectiveDiscount(x.order, x.os));
            PluginContext.Operations.AddButtonToOrderEditScreen("Discount Last",
                x => DiscountSystem.AddSelectiveDiscount1(x.order, x.os));
            PluginContext.Operations.AddButtonToOrderEditScreen("List Discount",
                x => DiscountSystem.AddSelectiveDiscount2(x.order, x.os));
            PluginContext.Log.InfoFormat("Plugin: was successfully registered");
        }

        public void Dispose()
        {
                foreach (var subscription in subscriptions)
                    if (subscription != null)
                        subscription.Dispose();
        }
    }
}