using System;
using System.Collections.Generic;
using System.Linq;
using Resto.Front.Api.Attributes.JetBrains;
using Resto.Front.Api.Data.Orders;
using Resto.Front.Api.Data.Security;
using Resto.Front.Api.Editors.Stubs;
using Resto.Front.Api.Exceptions;

namespace Resto.Front.Api.SampleScalePlugin
{
    public static class DiscountSystem
    {
        private const string Pin = "12344321";

        public static void AddSelectiveDiscount([NotNull] IOrder order, [NotNull] IOperationService os)
        {
            var selectedDish = new List<IOrderProductItemStub> { order.Items.OfType<IOrderProductItem>().First() };
            var discountType = os.GetDiscountTypes().Last(x => !x.Deleted && x.IsActive && x.CanApplySelectively);

            var editSession = os.CreateEditSession();
            editSession.AddDiscount(discountType, order);
            editSession.ChangeSelectiveDiscount(order, discountType, selectedDish, null, null);
            var creds = GetCredentials(os);
            os.SubmitChanges(editSession, creds);
        }
        
        public static void AddSelectiveDiscount1([NotNull] IOrder order, [NotNull] IOperationService os)
        {
            var selectedDish = new List<IOrderProductItemStub> { order.Items.OfType<IOrderProductItem>().Last() };
            var discountType = os.GetDiscountTypes().First(x => !x.Deleted && x.IsActive && x.CanApplySelectively);

            var editSession = os.CreateEditSession();
            editSession.AddDiscount(discountType, order);
            editSession.ChangeSelectiveDiscount(order, discountType, selectedDish, null, null);
            var creds = GetCredentials(os);
            os.SubmitChanges(editSession, creds);
        }
        
        public static void AddSelectiveDiscount2([NotNull] IOrder order, [NotNull] IOperationService os)
        {
            var tupleList = new List<(Guid, decimal)> {};
            foreach (var item in order.Items)
            {
                tupleList.Add((item.Id, 10));
                PluginContext.Log.InfoFormat($"{item.Id}");
            }
            PluginContext.Log.InfoFormat(tupleList.ToString());
            var readonlyList = tupleList.ToList().AsReadOnly();
            var discountType = os.GetDiscountTypes().Last(x => !x.Deleted && x.IsActive && x.CanApplySelectively);

            var editSession = os.CreateEditSession();
            editSession.AddDiscount(discountType, order);
            editSession.ChangeSelectiveDiscount(readonlyList, order, discountType);
            var creds = GetCredentials(os);
            os.SubmitChanges(editSession, creds);
        }
        

        [NotNull]
        private static ICredentials GetCredentials([NotNull] this IOperationService operationService)
        {
            if (operationService == null)
                throw new ArgumentNullException(nameof(operationService));

            try
            {
                return operationService.AuthenticateByPin(Pin);
            }
            catch (AuthenticationException)
            {
                PluginContext.Log.Warn("Cannot authenticate. Check pin for plugin user.");
                throw;
            }
        }
    }
}